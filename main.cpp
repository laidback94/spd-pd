#include <fstream>
#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <cmath>
#include <ctime>

int tasks_number;

struct Task
{
	int p, w, d;
};

bool load_data(std::string file_name, std::vector<Task>& data);
void show_data(const std::vector<Task>& data);
int single_task_punishment(const std::vector<Task>& data, int task_nr, int current_time);
void PD(const std::vector<Task>& data);
inline void startTimer();
inline void stopTimer();
inline double diffTimeMs();

std::clock_t time_start, time_stop;

int main()
{
	std::vector<Task> data;
	if(!load_data("data.txt", data))
	{
		std::cout<<"Blad otwarcia pliku \n";
		exit (-1);
	}

	startTimer();
	PD(data);
	stopTimer();

	std::cout<<"Czas wykonania programu: "<<diffTimeMs()<<" ms \n";
}

void PD(const std::vector<Task>& data)
{
	int permutations_number = pow(2, tasks_number);
	int *path = new int[permutations_number]; // co szukania zadan "od tylu"
	int *values = new int[permutations_number]; //wartosci funkcji celu dla kolejnych permutacji
	int *order = new int[tasks_number]; //kolejnosc wykonywanych zadan

	values[0]=0; //zalozenie poczatkowe

	int prev_index; //indeksy poprzednikow do liczenia c_max
	int end_task_index; //pozycja zadania, ktore idzie na koniec
	int c_max; //czas wykonania dla konkretnej permutacji
	int min_cost; //koszt poprzedniej badanej permutacji (jest minimalizowany, wiec poczotkowa wartosc jest duza)
	int min_cost_index; // indeks permutacji dla ktorej funkcja celu miala minimalna wartosc

	for(int permut_nr=1; permut_nr<permutations_number;++permut_nr) //po wszystkich permutacjach
	{
		// liczenie c_max
		prev_index=0;
		c_max=0;
		for(int mask=1;mask<=permut_nr; mask*=2, ++prev_index)
		{
			if(permut_nr&mask) //ktore zadania skladaja sie na czas wykonania
				c_max+=data[prev_index].p;
		}

		end_task_index=0;
		min_cost=99999;
		for(int mask=1; mask<=permut_nr; mask*=2, ++end_task_index) // minimalizacja funkcji celu
		{
			if( (permut_nr&mask) == 0) //ta kombinacja nie nalezy
				continue;

			int end_permut = permut_nr&(~mask); //permutacja, ktora testujemy na koncu

			int cost = std::min(min_cost, values[end_permut]+single_task_punishment(data, end_task_index, c_max));

			if(cost < min_cost) // jesli udalo nam sie znalesc mniejszy koszt
			{
				min_cost = cost;
				min_cost_index = end_task_index;
			}
		}
		path[permut_nr] = min_cost_index;
		values[permut_nr] = min_cost;
	}

	int task_index = permutations_number-1;
	int mask;

	for(int task_nr=0;task_nr<tasks_number;++task_nr)
	{
		order[tasks_number-task_nr-1] = path[task_index]+1;
		mask=1<<path[task_index];
		task_index = task_index&(~mask);
	}

	//WYNIKI
	std::cout<<"opt: "<<std::endl<<values[permutations_number-1]<<std::endl;
	for(int i=0;i<tasks_number;++i)
		std::cout<<order[i]<<" ";
	std::cout<<"\n";

	//Sprzatanie
	delete[] path;
	delete[] values;
	delete[] order;

}

int single_task_punishment(const std::vector<Task>& data, int task_nr, int current_time)
{
	int delay_time = std::max(0, current_time - data[task_nr].d);
	return delay_time*data[task_nr].w; //kara dla zadania task_nr gdy sie skonczy w chwili current_time
}

void show_data(const std::vector<Task>& data)
{
	for(unsigned int i=0;i<data.size();i++)
	{
		std::cout<<data[i].p<<" "<<data[i].w<<" "<<data[i].d<<"\n";
	}
}

bool load_data(std::string file_name, std::vector<Task>& data)
{
	std::ifstream file;
    file.open( file_name.c_str() );
    if( !file.good() )
         return false;

	Task t;

	file>>tasks_number;

    for(int i=tasks_number;i>0;--i)
    {
		file>>t.p>>t.w>>t.d;
		data.push_back(t);
	}
    file.close();

    return true;
}

inline void startTimer() {
	time_start = std::clock();
}

inline void stopTimer() {
	time_stop = std::clock();
}

inline double diffTimeMs() {
	if((double)time_stop >= (double)time_start) {
		return (time_stop - time_start) / (double)(CLOCKS_PER_SEC/1000);
	}
	return 0;
}
